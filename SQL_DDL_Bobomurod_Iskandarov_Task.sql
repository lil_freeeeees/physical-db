CREATE DATABASE BOB;

CREATE SCHEMA BobSchema;

CREATE TABLE IF NOT EXISTS Customers (
    customer_id INT PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL,
    phone_number VARCHAR(20) NOT NULL,
    address VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS Manufacturers (
    manufacturer_id INT PRIMARY KEY,
    manufacturer_name VARCHAR(100) NOT NULL,
    country VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS Categories (
    category_id INT PRIMARY KEY,
    category_name VARCHAR(50) NOT NULL,
    description TEXT
);

CREATE TABLE IF NOT EXISTS Products (
    product_id INT PRIMARY KEY,
    product_name VARCHAR(100) NOT NULL,
    manufacturer_id INT,
    category_id INT,
    price DECIMAL(10, 2),
    quantity INT DEFAULT 0,
    FOREIGN KEY (manufacturer_id) REFERENCES Manufacturers(manufacturer_id),
    FOREIGN KEY (category_id) REFERENCES Categories(category_id)
);

CREATE TABLE IF NOT EXISTS Orders (
    order_id INT PRIMARY KEY,
    customer_id INT,
    order_date DATE NOT NULL,
    total_price DECIMAL(10, 2),
    FOREIGN KEY (customer_id) REFERENCES Customers(customer_id)
);

CREATE TABLE IF NOT EXISTS Order_Details (
    order_detail_id INT PRIMARY KEY,
    order_id INT,
    product_id INT,
    quantity INT,
    price DECIMAL(10, 2),
    FOREIGN KEY (order_id) REFERENCES Orders(order_id),
    FOREIGN KEY (product_id) REFERENCES Products(product_id)
);

ALTER TABLE Customers
ADD CONSTRAINT CHECK_Email UNIQUE (email);

ALTER TABLE Products
ADD CONSTRAINT CHECK_Price CHECK (price >= 0);

ALTER TABLE Orders
ADD CONSTRAINT CHECK_OrderDate CHECK (order_date > '2000-01-01');

ALTER TABLE Orders
ADD CONSTRAINT CHECK_TotalPrice CHECK (total_price >= 0);

-- Populate tables with sample data
INSERT INTO Customers (customer_id, first_name, last_name, email, phone_number, address)
VALUES
    (1, 'Mr', 'Joshua', 'joshlovesdogs@gmail.com', '1234567890', '123 Main Street'),
    (2, 'Miss', 'Kate', 'kit.Kate@gmail.com', '4665498754', '456 Elm Avenue');

INSERT INTO Manufacturers (manufacturer_id, manufacturer_name, country)
VALUES
    (1, 'Samsung', 'South Korea'),
    (2, 'Apple', 'United States');

INSERT INTO Categories (category_id, category_name, description)
VALUES
    (1, 'Laptops', 'Portable computers'),
    (2, 'Smartphones', 'Advanced mobile phones');

INSERT INTO Products (product_id, product_name, manufacturer_id, category_id, price, quantity)
VALUES
    (1, 'Samsung Galaxy Book S', 1, 1, 999.99, 10),
    (2, 'Apple iPhone 12 Pro', 2, 2, 1299.99, 5);

INSERT INTO Orders (order_id, customer_id, order_date, total_price)
VALUES
    (1, 1, '2022-01-01', 999.99),
    (2, 2, '2022-01-02', 2599.97);

INSERT INTO Order_Details (order_detail_id, order_id, product_id, quantity, price)
VALUES
    (1, 1, 1, 1, 999.99),
    (2, 2, 2, 2, 2599.97);

-- Add record_ts field to tables
ALTER TABLE Customers
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE Manufacturers
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE Categories
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE Products
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE Orders
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE Order_Details
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

SELECT * FROM Customers;
SELECT * FROM Manufacturers;
SELECT * FROM Categories;
SELECT * FROM Products;
SELECT * FROM Orders;
SELECT * FROM Order_Details;